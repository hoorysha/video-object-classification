<h1>Video Object Classifier Project </h1>

<h3>running:</h3> 
<b>required arguments</b> <br>
--videoName - name of video <br>
--pathIn - path to video <br>
--spaceTime - requested time between each frame <br>
--target - the target object <br>
--model - object classification model <br>

<b>available videos:</b>
- car (one instance) </br>
- dogs (few instances) </br>

<b>for example:</b> python main.py --videoName car --pathIn videos/car.mp4 --spaceTime 1 --target car --model yolo
