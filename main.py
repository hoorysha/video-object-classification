import argparse
from functools import reduce

import cv2
import os
import matplotlib.pyplot as plt
import yolo


def classify_objects(image, model):
    if model == 'yolo':
        return yolo.detect(image)


def get_target_id(target, model):
    if model == 'yolo':
        return yolo.retrieve_target(target)


# creates tuple (time, [conf0, conf1, ...]) and saves it in result_list.
# each tuple contains the time(count) and list of the confidences of all appearances of the target object in the
# frame(of curr time).
# for example: [(0, [0.991]), (1, [0.5, 0.99])]
def save_confidences(result_list, class_ids, confidences, time, target):
    indices_of_target_object = [idx for idx, id in enumerate(class_ids) if id == target]  # indices of target object
    confidences_of_target = [confidences[id] for id in indices_of_target_object]
    result_tuple = (time, confidences_of_target)
    result_list.append(result_tuple)


# example:
# x_lists = [[0, 1], [0, 1]]
# y_lists = [[0.991, 0.5], [0, 0.99]]
def prepare_data(target_confidences_by_time):
    max_len = reduce((lambda max, tuple: len(tuple[1]) if (len(tuple[1]) > max) else max), target_confidences_by_time,
                     0)  # todo: check this!
    padded_tuples = [(tuple[0], tuple[1] + [0] * (max_len - len(tuple[1]))) for tuple in
                     target_confidences_by_time]  # pad the
    # tuples with zeros so that they all will be in the same size (max_len)
    x_lists = []
    y_lists = []
    for i in range(max_len):
        xlst = []
        ylst = []
        for tuple in padded_tuples:
            xlst.append(tuple[0])
            ylst.append(tuple[1][i])
        x_lists.append(xlst)
        y_lists.append(ylst)
    return x_lists, y_lists


def draw_graph(target_confidences_by_time, target):
    x_lists, y_lists = prepare_data(target_confidences_by_time)

    legend_lst = []
    for i in range(len(x_lists)):
        curr_xs = x_lists[i]
        curr_ys = y_lists[i]
        legend_lst.append('instance #%d' % i)
        plt.plot(curr_xs, curr_ys, '-o')
    plt.title('Found instances of %s' % target)
    plt.legend(legend_lst)
    plt.show()


def extract_images(video_name, path_in, space_time, target, model):
    count = 0
    imgs_list = []
    vidcap = cv2.VideoCapture(path_in)
    target_id = get_target_id(target, model)
    target_confidences_by_time = []  # [(time, [conf0, conf1, ...])] - list of tuples,

    # create the images folder
    # path_out = 'images/' + video_name + '_cropped_images'
    # os.mkdir(path_out)

    success, image = vidcap.read()
    while success:
        imgs_list.append(image)
        # cv2.imwrite(path_out + "\\frame%f.jpg" % count, image)  # save frame as JPEG file
        print('Read a new frame: ', success)
        boxes, confidences, classIDs = classify_objects(image, model)  # collect boxes dimensions, confidences and ids
        # of the detected objects in the current frame
        save_confidences(target_confidences_by_time, classIDs, confidences, count, target_id)
        count = count + space_time
        vidcap.set(cv2.CAP_PROP_POS_MSEC, (count * 1000))  # milliseconds
        # todo: can set the high&width of images, follow documentation of videocapture.set()
        success, image = vidcap.read()

    if not success:
        print('did not succeed to take frame')
    print('number of captured images: %d' % len(imgs_list))

    draw_graph(target_confidences_by_time, target)
    return imgs_list


if __name__ == "__main__":
    # input handling:
    a = argparse.ArgumentParser()
    a.add_argument("--videoName", help="name of video")
    a.add_argument("--pathIn", help="path to video")
    a.add_argument("--spaceTime", help="time between each frame")
    a.add_argument("--target", help="the target object")
    a.add_argument("--model", help="object classification model")
    args = a.parse_args()
    extract_images(args.videoName, args.pathIn, float(args.spaceTime), args.target, args.model)
    # extract_images('dogs', 'videos/dogs.mp4', 1, 'car', 'yolo')
# example: python main.py --videoName car --pathIn videos/car.mp4 --spaceTime 1 --target car --model yolo
